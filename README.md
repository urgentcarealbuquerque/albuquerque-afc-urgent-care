**Albuquerque afc urgent care**

Ensuring Your Precious Ones' Wellbeing in One Place 
Wouldn't it be nice to know that when they get sick or ill, there's one spot you can count on to take care of any member of your family? 
At AFC Emergency Care Albuquerque, our talented medical team is willing to deliver non-emergency medical care to people of all ages.
Please Visit Our Website [Albuquerque afc urgent care](https://urgentcarealbuquerque.com/afc-urgent-care.php) for more information. 

---

## Our afc urgent care in Albuquerque 

You can trust our AFC Health Care Specialist medical providers to have all the medicines and vaccines that your family needs, 
if your child has an illness, your oldest relative has broken a leg, or if you have signs of flu. 
AFC Emergency Care Albuquerque is proud to provide a range of programs to people of all ages, such as:
Physical assessments 
Routine reviews 
X-ray 
Primary Treatment Provider 
Emergency medical programs 
Flu shots and vaccination 
No matter what your reasons for visiting us might be, you can depend on the best possible services provided by Albuquerque emergency care AFC. 
Walk-ins are always welcome and, for your ease and peace of mind, we welcome most forms of insurance.

